package walmoo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by KalvisKol on 7/6/2015.
 */
public class BaseRequest {

    // Input objects
    String loginHash;
    String username;
    String password;
    String clientIP;
    int operation;

    // Output objects
    int status;
    String statusMSG;
    HashMap<String, String> data;


    public BaseRequest (String login_hash, String username, String paswd, int operation, String CLIENT_IP) {
        this.loginHash = login_hash;
        this.username = username;
        this.password = paswd;
        this.operation = operation;
        this.clientIP = CLIENT_IP;
    }

    public static void sendPost() throws Exception {
        URL url = new URL("http://dev.api.walmoo.com/api/web_output.php");
        String postData = "?mobile=false&data={\"login_hash\":\"HR38S12EW983L46839476I6L07J4W49Z5B951Y5V\",\"username\":\"kalvis_test\",\"operation\":2,\"paswd\":\"q3563bae461243vbcvb4\",\"CLIENT_IP\":\"127.0.0.1\"}";
        byte[] postDataBytes = postData.getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "form-data");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        // Nav zin?ms Outputs
//       System.out.print(conn.getOutputStream().toString());
//        BufferedReader br = null;
//        br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//        StringBuilder builder = new StringBuilder();
//        String output;
//        while ((output = br.readLine()) != null){
//            builder.append(output);
//        }
//
//        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
//        for ( int c = in.read(); c != -1; c = in.read() )
//            System.out.print((char)c);

        conn.disconnect();
    }


    public String toString() {
        return "Login Hash: " + loginHash + "Username: " + username + " Password: " + password + " Operation: " + operation + " ClientIP: " + clientIP;
    }

    public String getUsername() {
        return username;
    }

}
