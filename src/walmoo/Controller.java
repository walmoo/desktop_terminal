package walmoo;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;;
import java.net.*;
import java.io.*;
import java.util.*;

public class Controller extends AnchorPane{
    @FXML
    private TextField emailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label alertLabel;

    public void LoginButtonClick() {
        alertLabel.setText("");
        if(emailField.getText().equals("") || passwordField.getText().equals("") || !emailField.getText().contains("@") || !emailField.getText().contains(".")) {
            alertLabel.setText("Email or Password is incorrect!");
            return;
        }

        System.out.println(emailField.getText());
        System.out.println(passwordField.getText());
    }

    public void RegisterButtonClick() {
        try {
            BaseRequest.sendPost();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        }
        catch (Exception e) {
            System.out.println("Error1");
            result = null;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    System.out.println("Error2");
                }
            }
        }

        return result;
    }
}


//String urlParameters  = "param1=a&param2=b&param3=c";
//byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
//int    postDataLength = postData.length;
//String request        = "http://example.com/index.php";
//URL    url            = new URL( request );
//HttpURLConnection conn= (HttpURLConnection) url.openConnection();
//conn.setDoOutput( true );
//        conn.setInstanceFollowRedirects( false );
//        conn.setRequestMethod( "POST" );
//        conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
//        conn.setRequestProperty( "charset", "utf-8");
//        conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
//        conn.setUseCaches( false );
//        try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
//        wr.write( postData );
//        }